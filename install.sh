#!/bin/bash

set -xe

mkdir -p /usr/local/include/xolatile

cp xlesses.h /usr/local/include/xolatile/xlesses.h

exit
