#include <xolatile/xlesses.h>

int main (void) {
	xlesses_configure (640, 480, "Heyo world!");

	while (xlesses_active == true) {
		xlesses_render_character ('H',           0xee2222, 0x222222, 64, 64);
		xlesses_render_string    ("ello world!", 0xeeeeee, 0x222222, 72, 64);

		xlesses_render_string (format ("Heyo %s%i!", "A", 6), 0x22ee22, 0x222222, 64, 72);

		xlesses_synchronize ();
	}

	return (log_success);
}
