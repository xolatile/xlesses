#include <xolatile/xlesses.h>

int main (void) {
	xlesses_configure (640, 480, "Heyo world!");

	while (xlesses_active == true) {
		xlesses_render_background_colour (0xff00ff);

		xlesses_synchronize ();
	}

	return (log_success);
}
